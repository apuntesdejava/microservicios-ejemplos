package com.apuntesdejava.jersey;

import java.net.URI;
import javax.ws.rs.core.UriBuilder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.simple.SimpleContainerFactory;
import org.glassfish.jersey.simple.SimpleServer;

/**
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
/* 
Usando    <dependency>
            <groupId>org.glassfish.jersey.containers</groupId>
            <artifactId>jersey-container-simple-http</artifactId>
        </dependency>
 */
public class SimpleServerMain {

    static final URI BASE_URI = UriBuilder.fromUri("http://localhost/").port(8002).build();

    public static SimpleServer startServer() {
        ResourceConfig config = new ResourceConfig(MyResource.class).packages("com.apuntesdejava.jersey");
        config.register(JacksonFeature.class); //para mostrar las listas por el Response
        SimpleServer server = SimpleContainerFactory.create(BASE_URI, config);
        server.setDebug(true);
        return server;
    }

    public static void main(String[] args) {
        SimpleServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit Ctrl+C to stop it...", BASE_URI));
    }
}
