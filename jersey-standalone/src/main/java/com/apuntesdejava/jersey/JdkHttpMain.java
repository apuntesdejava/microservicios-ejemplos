package com.apuntesdejava.jersey;

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.URI;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
/*
Usando
    <dependency>
            <groupId>org.glassfish.jersey.containers</groupId>
            <artifactId>jersey-container-jdk-http</artifactId>
        </dependency>
*/
public class JdkHttpMain {
    // Base URI the HttpServer HTTP server will listen on

    public static final String BASE_URI = "http://localhost:8001/jersey/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this
     * application.
     *
     * @return HTTP server.
     */
    public static HttpServer startServer() {
        final ResourceConfig rc = new ResourceConfig().packages("com.apuntesdejava.jersey");
        rc.register(JacksonFeature.class); //para mostrar las listas por el Response

        return JdkHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.stop(0);
    }
}
