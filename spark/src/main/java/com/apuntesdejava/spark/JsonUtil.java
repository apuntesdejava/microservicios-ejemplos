package com.apuntesdejava.spark;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 *
 * @author Diego Silva <diego.silva@apuntesdejava.com>
 */
public class JsonUtil {

    public static <T> String toJson(T e) {
        return new Gson().toJson(e);
    }

    public static ResponseTransformer json() {
        return JsonUtil::toJson;
    }
}
